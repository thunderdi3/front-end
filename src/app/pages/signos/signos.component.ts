import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Signos } from '../../_model/signos';
import { SignosService } from '../../_service/signos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  displayedColumns = ['paciente', 'fecha', 'temperatura', 'pulso', 'ritmorespiratorio', 'acciones'];

  dataSource: MatTableDataSource<Signos>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  cantidad: number;

  mensaje: string;

  constructor(private signosService: SignosService,
    public route: ActivatedRoute,
    public snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.signosService.signosCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.signosService.mensaje.subscribe(data => {
      this.snackBar.open(data, null, {
        duration: 2000,
      });
    });

    this.signosService.getlistar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  eliminar(Signos: Signos): void {
    this.signosService.eliminar(Signos).subscribe(data => {
      if (data === 1) {
        this.signosService.getlistar().subscribe(data => {
          this.signosService.signosCambio.next(data);
          this.signosService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.signosService.mensaje.next("No se pudo eliminar");
      }
    });
  }



  mostrarMas(e) {

    this.signosService.getlistarSignos(e.pageIndex, e.pageSize).subscribe(data => {
      let signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(signos);
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
}
