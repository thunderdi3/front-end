import { Paciente } from './../../../../../../../../angular6-mediapp-frontend/mediapp-frontend/src/app/_model/paciente';
import { PacienteService } from './../../../../_service/paciente.service';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-signos-paciente',
  templateUrl: './signos-paciente.component.html',
  styleUrls: ['./signos-paciente.component.css']
})
export class SignosPacienteComponent implements OnInit {

  id: number;
  paciente: Paciente;
  form: FormGroup;
  edicion: boolean = false;

  retornopaciente: Paciente;


  constructor(private pacienteService: PacienteService,
    private route: ActivatedRoute,
    private router: Router) {
    this.paciente = new Paciente();
    this.retornopaciente=new Paciente();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dni': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl('')
    });
  }

  ngOnInit() {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });


  }

  private initForm() {
    if (this.edicion) {
      this.pacienteService.getPacientePorId(this.id).subscribe(data => {
        let id = data.idPaciente;
        let nombres = data.nombres;
        let apellidos = data.apellidos;
        let dni = data.dni;
        let direccion = data.direccion;
        let telefono = data.telefono;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombres': new FormControl(nombres),
          'apellidos': new FormControl(apellidos),
          'dni': new FormControl(dni),
          'direccion': new FormControl(direccion),
          'telefono': new FormControl(telefono)
        });
      });



    }
  }

  operar() {
    this.paciente.idPaciente = this.form.value['id'];
    this.paciente.nombres = this.form.value['nombres'];
    this.paciente.apellidos = this.form.value['apellidos'];
    this.paciente.dni = this.form.value['dni'];
    this.paciente.direccion = this.form.value['direccion'];
    this.paciente.telefono = this.form.value['telefono'];

    if (this.edicion) {
      //update
      this.pacienteService.modificar(this.paciente).subscribe(data => {
        if (data === 1) {
          this.pacienteService.getlistarPaciente(0, 100).subscribe(pacientes => {
            this.pacienteService.pacienteCambio.next(pacientes);
            this.pacienteService.mensaje.next('Se modificó');
          });
        } else {
          this.pacienteService.mensaje.next('No se modificó');
        }
      });
    } else {
      //insert  que retorna el id del paciente creado
      this.pacienteService.registrardata(this.paciente).subscribe(data => {

        if (data > 0) {
          let idpac: any;
          idpac = data;
          //CREACION CE NUEVO PACIENTE PARA ALMACENAR TODOS LOS DATOS GENERADOS
          this.retornopaciente.idPaciente = idpac;
          this.retornopaciente.nombres = this.paciente.nombres;
          this.retornopaciente.apellidos = this.paciente.apellidos;
          this.retornopaciente.telefono = this.paciente.telefono;
          this.retornopaciente.direccion = this.paciente.direccion;
          this.retornopaciente.dni = this.paciente.dni;

          this.pacienteService.pacienteCreado.next(this.retornopaciente);

          this.pacienteService.getlistarPaciente(0, 100).subscribe(pacientes => {
            this.pacienteService.pacienteCambio.next(pacientes);
            this.pacienteService.mensaje.next('Se registró');
          });
        } else {
          this.pacienteService.mensaje.next('No se registró');
        }
      });
    }

    this.router.navigate(['signos/nuevo'])
  }

}
