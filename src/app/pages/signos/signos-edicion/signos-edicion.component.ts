import { SignosService } from './../../../_service/signos.service';
import { Signos } from '../../../_model/signos';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Paciente } from '../../../_model/paciente';
import { PacienteService } from '../../../_service/paciente.service';
import { map, startWith } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})

export class SignosEdicionComponent implements OnInit {

  //myControl: FormControl = new FormControl();
  pacienteSeleccionado: Paciente;
  pacientes: Paciente[] = [];
  filteredOptions: Observable<any[]>;

  //vpaciente: Paciente;

  id: number;
  signos: Signos;
  form: FormGroup;
  edicion: boolean = false;

  isEdicion: boolean = true;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date()

  constructor(private signosService: SignosService, private route: ActivatedRoute,
    private router: Router, private pacienteService: PacienteService,
    private snackBar: MatSnackBar) {

    this.signos = new Signos();

    //this.vpaciente = new Paciente();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': new FormControl(),
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmorespiratorio': new FormControl('')
    });
  }

  ngOnInit() {

    this.pacienteService.pacienteCreado.subscribe(data => {
      this.pacienteSeleccionado = data;
      this.initForm();
    });

    this.pacienteService.pacienteCambio.subscribe(data => {
      let patcambio = JSON.parse(JSON.stringify(data)).content;
      this.pacientes = patcambio;
    });

    this.listarPacientes();

    this.filteredOptions = this.form.controls.paciente.valueChanges
      .pipe(
        startWith(null),
        map(val => this.filter(val))
      );

    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });




    this.pacienteService.mensaje.subscribe(data => {
      this.snackBar.open(data, null, { duration: 2000 });
    });




  }

  filter(val: any) {
    if (val != null) {
      if (val != null && val.idPaciente > 0) {
        return this.pacientes.filter(option =>
          option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
      } else {
        return this.pacientes.filter(option =>
          option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
      }
    }
  }

  displayFn(val: Paciente) {
    let retorno: any;
    if (val != null) {
      retorno = val ? `${val.nombres} ${val.apellidos}` : val;
    }
    return retorno;
  }

  seleccionarPaciente(e) {
    this.pacienteSeleccionado = e.option.value;
  }

  listarPacientes() {
    this.pacienteService.getlistar().subscribe(data => {
      this.pacientes = data;
    });
  }

  private initForm() {
    if (this.edicion) {
      this.signosService.getSignoPorId(this.id).subscribe(data => {
        let vid = data.idSignos;
        let vpat = data.paciente;
        let vfecha = data.fecha;
        let vtemperatura = data.temperatura;
        let vpulso = data.pulso;
        let vritmores = data.ritmorespiratorio;

        this.fechaSeleccionada = vfecha;
        this.pacienteSeleccionado = vpat;
        this.isEdicion = true;

        this.form = new FormGroup({
          'id': new FormControl(vid),
          'paciente': new FormControl(vpat),
          'fecha': new FormControl(new Date(vfecha)),
          'temperatura': new FormControl(vtemperatura),
          'pulso': new FormControl(vpulso),
          'ritmorespiratorio': new FormControl(vritmores)

        });
      });
    } else {

      if (this.pacienteSeleccionado != null) {
        this.form = new FormGroup({
          'id': new FormControl(0),
          'paciente': new FormControl(this.pacienteSeleccionado),
          'fecha': new FormControl(new Date()),
          'temperatura': new FormControl(''),
          'pulso': new FormControl(''),
          'ritmorespiratorio': new FormControl('')

        });
      }


      this.isEdicion = false;
    }
  }

  getActivar() {
    return this.isEdicion;
  }


  operar() {
    this.signos.idSignos = this.form.value['id'];
    this.signos.paciente = this.form.value['paciente'];
    this.signos.fecha = this.form.value['fecha'];
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.ritmorespiratorio = this.form.value['ritmorespiratorio'];

    if (this.edicion) {

      if (this.signos != null && this.signos.idSignos > 0) {
        this.signosService.modificar(this.signos).subscribe(data => {
          if (data === 1) {
            this.signosService.getlistar().subscribe(signos => {
              this.signosService.signosCambio.next(signos);
              this.signosService.mensaje.next("Se modifico");
            });
          } else {
            this.signosService.mensaje.next("No se pudo modificar");
          }
        });
      }

    } else {

      this.signosService.registrar(this.signos).subscribe(data => {
        if (data === 1) {
          this.signosService.getlistar().subscribe(signos => {
            this.signosService.signosCambio.next(signos);
            this.signosService.mensaje.next("Se registro");
          });
        } else {
          this.signosService.mensaje.next("No se pudo registrar");
        }
      });


    }

    this.router.navigate(['signos']);
  }




}
